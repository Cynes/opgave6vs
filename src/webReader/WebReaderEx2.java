package webReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

public class WebReaderEx2 {

	public static void main(String args[]) throws IOException {

		//exercise1();
		exercise2();

	}

	public static void exercise1() throws IOException {
		URL url = new URL("http://cnds.students.dk/example3.php");
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write("year=2009&month=august");
		wr.flush();
		InputStream stream = conn.getInputStream();
		InputStreamReader isr = new InputStreamReader(stream);
		BufferedReader in = new BufferedReader(isr);
		String str;

		while ((str = in.readLine()) != null) {
			/*
			 * Beh�ves kun hvis man ikke gider tags
			 * str = str.replaceAll("<[^>]*>", "");
			 */
			System.out.println(str);

		}

		wr.close();
	}

	public static void exercise2() throws IOException {
		URL url = new URL("http://cnds.students.dk/example3.php");
		URLConnection conn = url.openConnection();
		conn.setDoOutput(true);
		OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
		wr.write("year=2009&month=august");
		wr.flush();
		InputStream stream = conn.getInputStream();
		InputStreamReader isr = new InputStreamReader(stream);
		BufferedReader in = new BufferedReader(isr);
		Map map = conn.getHeaderFields();
		Set set = map.entrySet();
		ArrayList<String> list = new ArrayList<>();
		Iterator iterator = set.iterator();
		while (iterator.hasNext()) {
			list.add(iterator.next().toString());
			wr.close();
		}
		int count = 0;
		for(String s : list) {
			if(count>2) {
				System.out.println(s);
			}
			count++;
		}
	}
}
