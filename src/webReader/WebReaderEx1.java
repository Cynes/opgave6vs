package webReader;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;

public class WebReaderEx1 {


	public static void main(String args[]) throws IOException {
		URL url = new URL("http://dis.students.dk/example1.php");
		InputStreamReader r = new InputStreamReader(url.openStream());
		BufferedReader in = new BufferedReader(r);
		String str;
		int count;
		while ((str = in.readLine()) != null) {
			if(str.matches("<body>")){
				str = in.readLine();
				str = str.replaceAll("\\D+","");
				System.out.println(str);
			}
		}
		in.close();
	}
}
